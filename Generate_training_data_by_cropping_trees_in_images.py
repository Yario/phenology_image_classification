#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generate training data by cropping single trees with a mask
Subfolders must comtainn a "mask.png" image

Created on Fri May  6 11:03:16 2022

@author: mario
"""

import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
from PIL import ExifTags
from random import choice

''' 01 - Settings
----------------------------------------------------------------------------'''
IMG_SIZE = (1920, 2560)
CROP_SIZE =  512

''' 02 - Functions to do the cropping and augmentation
----------------------------------------------------------------------------'''
def mask_processing(mask_dir):
  mask = plt.imread(mask_dir)
  axis=2
  # Check how many masks exist (number of colors/trees in mask.png)
  mask_rgb = np.unique(
          mask.view(np.dtype((np.void, mask.dtype.itemsize*mask.shape[axis])))
          ).view(mask.dtype).reshape(-1, mask.shape[axis])
  ## remove the background color (all zero)), because it is not a tree-mask
  check_zero_background = [ not all(mask_rgb[i,:] == 0) for i in range(mask_rgb.shape[0]) ]
  mask_rgb = mask_rgb[check_zero_background, ]
  n_mask = mask_rgb.shape[0]
  mask_list = []
  for mask_i in range(n_mask):
    print("Processing mask " + str(mask_i+1) + "/" + str(n_mask) )
    crop_i_boolean = np.all(mask == mask_rgb[mask_i], axis=2)
    crop_i_coordinates = np.where(crop_i_boolean)
    # Create tuple with pixels to crop for tf.keras.layers.Cropping2D
    crop_tuple = ((min(crop_i_coordinates[0]),                  # top
                  IMG_SIZE[0] - max(crop_i_coordinates[0])-1) , # bottom
                    (min(crop_i_coordinates[1]),                # left
                    IMG_SIZE[1] - max(crop_i_coordinates[1])-1) # right
                   )
    # set mask to the same dimensions as image (for dot product with mask/zero)
    crop_i = tf.where(crop_i_boolean, 1, 0)
    crop_i = tf.cast(crop_i, dtype=tf.float32)
    crop_i = tf.stack([crop_i,crop_i,crop_i])
    crop_i = tf.keras.backend.permute_dimensions(crop_i, (1,2,0))
    crop_i = tf.expand_dims(crop_i, 0)
    mask_list.append((crop_tuple, crop_i))

  ## Create a new image with all masks and print the mask ID inside each mask
  plt.figure()
  plt.imshow(mask)
  for mask_i in range(n_mask):
      ## get the center coordinates of each mask
      x = mask_list[mask_i][0][1][0] + (IMG_SIZE[1] - mask_list[mask_i][0][1][0] - mask_list[mask_i][0][1][1]) / 2
      y = mask_list[mask_i][0][0][0] + (IMG_SIZE[0] - mask_list[mask_i][0][0][0] - mask_list[mask_i][0][0][1] ) / 2
      ## Plot and save the mask image with IDs
      plt.text(x,y,mask_i+1)
      plt.axis('off')
      plt.savefig( mask_dir.replace("mask.png","mask_ID.png") ) # save under new name
      

  return(mask_list) # output is a list; each list item is a tuple with 2 items: a tuple to crop the image and the mask array


'''-------------------------------------------------------------------------'''
def img_crop(img, mask_tuple):
  output = tf.math.multiply(img, mask_tuple[1])
  ## crop the camera trap's meta data at the bottom
  output = tf.keras.layers.Cropping2D(cropping = mask_tuple[0])(output)
  ## the model expects 512x512 image, crop and resize without distortion
  output = tf.image.resize_with_pad(image = output,
                                   target_height = CROP_SIZE,
                                   target_width = CROP_SIZE)

  return(output)


''' Find all directories that contain a mask.png '''
'''-------------------------------------------------------------------------'''
def crop_all_img_in_dir(INPUT_DIR, SAVE_DIR):
  ## Three images per day are taken at different times.
  ## They will be randomly assigned to one of the subdirectories:
  ## 3 subdirectories are created if they do not yet exist: train, validation, method_comparison.
  sub_dataset_names = ["train", "validation", "method_comparison"]
  subdir_list = list(os.walk(SAVE_DIR))[0][1]
  for save_subdirs in sub_dataset_names:
      if not save_subdirs in subdir_list:
          os.mkdir(os.path.join(SAVE_DIR, save_subdirs))
  
  ## Initiate a pandas dataframe for image file name, date + time and sub-dataset
  img_df = pd.DataFrame(columns=["img","time","dataset"])  
  
  cam_dir_list = []
  cam_name_list = []
  for root, dirs, files in os.walk(INPUT_DIR):
    if "mask.png" in files:
      cam_dir_list.append(root)
      cam_name_list.append( os.path.basename( root ) )
  
  for cam_dir, cam in zip(cam_dir_list, cam_name_list):
    print("Processing camera directory: " + cam)
    ## Get mask
    mask_dir = os.path.join(cam_dir, "mask.png")
    mask_list = mask_processing(mask_dir)
    ## Get image list
    img_list = [os.path.join(dp, f) for dp, dn, filenames in os.walk(cam_dir) for f in filenames if os.path.splitext(f)[1] == '.JPG']
    image_N = str(len(img_list))
    
    ## Loop over images
    for img_i, img_path in enumerate(img_list):
      if img_i % 10 == 0:
        print("Processing cam " + cam + " and image " + str(img_i + 1) + "/" + image_N )
      img = tf.keras.preprocessing.image.load_img(img_path, target_size=IMG_SIZE)
      img_array = tf.keras.preprocessing.image.img_to_array(img)
      img_array = tf.expand_dims(img_array, 0)  # Create batch axis    
      
      ## Read date from exif meta data and assign image to one sub-dataset (train(validation/method comparison))
      img_date = { ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS }['DateTime']
      ## Three images per day should exist from different times.
      ## Each image sould be assigned randomly to the train, validation or method comparison dataset.
      time_check = img_date[0:10] # date string
      if any(time_check in string for string in list(img_df.time)):
          # find potential other images with this date
          date_i = np.where([time_check in string for string in img_df.time])
          ## merge existing dataset names with list of all dataset names,
          ## then only take the unique datasets (those that were not used for images from this date before)
          remaining_sets = img_df.dataset.iloc[date_i].tolist() + sub_dataset_names   #["test", "test2"] + ["test3", "test4"]
          remaining_sets = [x for x in remaining_sets if remaining_sets.count(x)==1]
          if len(remaining_sets) != 0:
              selected_set = choice(remaining_sets)
          else:
              selected_set = choice(sub_dataset_names)
          img_df.loc[len(img_df)] = [img_path, img_date, selected_set] # append new image's information
      else:
          selected_set = choice(sub_dataset_names)
          img_df.loc[len(img_df)] = [img_path, img_date, selected_set] # append new image's information

      ## Apply the mask and the image
      for mask_i in range(len(mask_list)):
        # time_series["Score_" + str(mask_i)] = None
        cropped_img = img_crop(img=img, mask_tuple=mask_list[mask_i])
        ## Save croppedimage
        original_image_id = os.path.basename(img_path).split(".")[0]
        tf.keras.utils.save_img(
          # path = SAVE_DIR + "/" + cam + "_"+ str(img_i + 1) + "_TreeMask_" + str(mask_i + 1) + "_cropped.JPG",
          path = os.path.join(SAVE_DIR, selected_set, "cam_" + cam + "_"+ "image_" + original_image_id + "_TreeMask_" + str(mask_i + 1) + ".JPG"),
          x = cropped_img[0]
          )
  ## Save image dataframe as csv
  img_df.to_csv(os.path.join(SAVE_DIR, "image_date_and_dataset.csv"))

''' 03 - Manually apply the function to a directory in this script
----------------------------------------------------------------------------'''
# crop_all_img_in_dir(INPUT_DIR = INPUT_DIR,
#                     SAVE_DIR = SAVE_DIR
#                     )

''' 04 - Simple QT GUI
----------------------------------------------------------------------------'''
import sys
# from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow, QFrame, QVBoxLayout, QPushButton, QFileDialog, QLabel

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.window_width, self.window_height = 300, 200
        self.setMinimumSize(self.window_width, self.window_height)
        self.move(300, 300)
        self.setStyleSheet("QLabel{font-size: 10pt;}")
        
        self.input_dir = None
        self.output_dir = None
        
        frame =QFrame()
        self.setCentralWidget(frame)
        layout = QVBoxLayout()
        frame.setLayout(layout)
        
        ''' Input label '''
        self.label_input = QLabel("Input directory: ")
        layout.addWidget(self.label_input)  
        ''' Input button '''
        self.ButtonOpen = QPushButton('Select INPUT directory',self)
        layout.addWidget(self.ButtonOpen)
        self.ButtonOpen.clicked.connect(self.buttonInputClicked)
        
        ''' Output label '''
        self.label_output = QLabel("Select output directory")
        layout.addWidget(self.label_output)  
        ''' Input button '''
        self.ButtonSave= QPushButton('Select OUTPUT directory',self)
        layout.addWidget(self.ButtonSave)
        self.ButtonSave.clicked.connect(self.buttonOutputClicked)
        
        ''' Start the cropping of the images '''
        self.label_run =QLabel("Crop all images with the tree masks")
        layout.addWidget(self.label_run)
        self.btn_analyze = QPushButton('Run')
        self.btn_analyze.clicked.connect(self.analyze_dir)
        layout.addWidget(self.btn_analyze)

    def buttonInputClicked(self):
      self.input_dir = QFileDialog.getExistingDirectory(
          self,
          caption='Select an INPUT folder'
      )
      if self.input_dir:
        self.label_input.setText("Input directory:\n" + self.input_dir)
        
    def buttonOutputClicked(self):
      # def getInputDirectory(self):
      self.output_dir = QFileDialog.getExistingDirectory(
          self,
          caption='Select an OUTPUT folder'
      )
      if self.input_dir:
        self.label_output.setText("Output directory:\n" + self.output_dir)
        
    def analyze_dir(self):
      print("Processing: " + self.input_dir)
      crop_all_img_in_dir(INPUT_DIR = self.input_dir,
                          SAVE_DIR = self.output_dir
                          )

def main():
    app = QApplication([])
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    CroppApp = MainWindow()
    CroppApp.show()

    try:
        sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')



