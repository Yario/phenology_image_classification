## Classifaction of phenological forest images
  
There are two tools in this repository:  
  
### 1) Phenology_Application.py
The application evaluates the images from pheno cams and returns time-series of the phenological stage and the RGB channels. The phenological stages are predicted with a simple artificial neural network (Xception). Additionally, the mean of each color channel (RGB) in the tree-masks is computed.  
![](GUI_Pheno_Application.png)

#### How to use it
- Start the script/program
- Select the directory with all your cameras
- Click "Analyze"
  
#### Requirements  
1. The selected directory must have **sub-directories**, each belonging to one camera.
2. Each camera sub-directory must contain a **mask.png** and a sub-directory that contains the forest images.
3. Images must contain the date in the EXIF metadata.  
![](pheno_dir_structure.png)

#### Output
The tool returns a time series of the phenological stages and the means of the RGB channels (plot and csv file) for each tree in each camera.   
The output is saved in the camera directory.  

| ![](example_time_series.jpg) |
|:--:| 
| *The model output is a time series of the phenological stages for each tree.* |


| ![](RGB_time_series.jpg) |
|:--:| 
| *Additonally, the means of the RGB channels are returned.* |

### 2) Generate_training_data_by_cropping_trees_in_images.py
This tool can be used to create the training data for the deep learning model.
In particular, it takes the **mask.png** image, in which all separate regions of interest (ROI, trees) are marked, and crops this tree out of the original images.  
![](GUI_crop.png)
  
#### How to use it
- Start the script/program
- Select the directory with all your cameras
- Select where you want to save the croped images (training data)
- Click "Run"

#### Requirements  
1. The selected directory must have **sub-directories**, each belonging to one camera.
2. Each camera sub-directory must contain a **mask.png** and a sub-directory that contains the forest images from this camera (see schemata above).
