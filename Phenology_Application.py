#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Apply model that detrkines the phenology from images


Created on Tue May  3 09:54:54 2022

@author: mario
"""

import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
from PIL import Image, ExifTags
from datetime import datetime
from pandas import read_csv


''' 01 - Settings
----------------------------------------------------------------------------'''
IMG_SIZE = (1920, 2560)
color_dict = {"autumn":"orange", "summer":"green","winter":"steelblue"}

''' 02 - Select and load model
----------------------------------------------------------------------------'''
def load_model(n_seasons):
    if n_seasons == "2 seasons":
        # get current dir; should both be cross platform compatible
        model_dir = os.path.dirname(os.path.realpath(__file__)) # this works in linux
        model_dir = os.path.join(model_dir, "2_seasons_model")
        model = tf.keras.models.load_model(model_dir)
        classes = ['summer', 'winter']
    
    if n_seasons == "3 seasons":
        # get current dir; should both be cross platform compatible
        model_dir = os.path.dirname(os.path.realpath(__file__)) # this works in linux
        model_dir = os.path.join(model_dir, "3_seasons_model")
        model = tf.keras.models.load_model( model_dir)
        classes = ['autumn', 'summer', 'winter']
    
    return model, classes

## For development: manual specification of the model path
# model_dir = "/home/mario/Dokumente/Python/Phenology_from_images/2_seasons_model"
# model_dir = "/home/mario/Dokumente/Python/Phenology_from_images/3_seasons_model"


''' 03 - Function to create one mask (tree) per color in the mask.png
----------------------------------------------------------------------------'''
def mask_processing(mask_dir):
  mask = plt.imread(mask_dir)
  axis=2
  # Check how many masks exist (number of colors/trees in mask.png)
  mask_rgb = np.unique(
          mask.view(np.dtype((np.void, mask.dtype.itemsize*mask.shape[axis])))
          ).view(mask.dtype).reshape(-1, mask.shape[axis])
  ## remove the background color (all zero)), because it is not a tree-mask
  check_zero_background = [ not all(mask_rgb[i,:] == 0) for i in range(mask_rgb.shape[0]) ]
  mask_rgb = mask_rgb[check_zero_background, ]
  n_mask = mask_rgb.shape[0]
  mask_list = []
  for mask_i in range(n_mask):
    print("Processing mask " + str(mask_i+1) + "/" + str(n_mask) )
    crop_i_boolean = np.all(mask == mask_rgb[mask_i], axis=2)
    crop_i_coordinates = np.where(crop_i_boolean)
    # Create tuple with pixels to crop for tf.keras.layers.Cropping2D
    crop_tuple = ((min(crop_i_coordinates[0]),                  # top
                  IMG_SIZE[0] - max(crop_i_coordinates[0])-1) , # bottom
                    (min(crop_i_coordinates[1]),                # left
                    IMG_SIZE[1] - max(crop_i_coordinates[1])-1) # right
                   )
    # set mask to the same dimensions as image (for dot product with mask/zero)
    crop_i = tf.where(crop_i_boolean, 1, 0)
    crop_i = tf.cast(crop_i, dtype=tf.float32)
    crop_i = tf.stack([crop_i,crop_i,crop_i])
    crop_i = tf.keras.backend.permute_dimensions(crop_i, (1,2,0))
    crop_i = tf.expand_dims(crop_i, 0)
    mask_list.append((crop_tuple, crop_i))
    
  ## Create a new image with all masks and print the mask ID inside each mask
  plt.figure()
  plt.imshow(mask)
  for mask_i in range(n_mask):
      ## get the center coordinates of each mask
      x = mask_list[mask_i][0][1][0] + (IMG_SIZE[1] - mask_list[mask_i][0][1][0] - mask_list[mask_i][0][1][1]) / 2
      y = mask_list[mask_i][0][0][0] + (IMG_SIZE[0] - mask_list[mask_i][0][0][0] - mask_list[mask_i][0][0][1] ) / 2
      ## Plot and save the mask image with IDs
      plt.text(x,y,mask_i+1)
      plt.axis('off')
      plt.savefig( mask_dir.replace("mask.png","mask_ID.png") ) # save under new name
    
  return(mask_list) # output is a list; each list item is a tuple with 2 items: a tuple to crop the image and the mask array

''' 04 -  Function to apply mask and crop image to 512x512
----------------------------------------------------------------------------'''
def img_crop(img, mask_tuple):
  ## sett all pixels except the maks to 0
  output = tf.math.multiply(img, mask_tuple[1])
  # crop the recatangle of the maks
  output = tf.keras.layers.Cropping2D(cropping = mask_tuple[0])(output)
  ## Crop and resize to 512x512
  output = tf.image.resize_with_pad(image = output,
                                  target_height = 512,
                                  target_width = 512)
  return(output)


''' 05 - Function that creates the phenology time-series
for each tree in a mask image.
Time-series values for: probability of each class/season + mean RGB values
----------------------------------------------------------------------------'''
img_dir = "/D/Work/Other_peoples_data/ExPlEco_phenology_pictures/inference_test_with_mask_2"
mask_dir = "/D/Work/Other_peoples_data/ExPlEco_phenology_pictures/inference_test_with_mask_2/mask.png"
def pheno_images_to_time_series_multi(model, img_dir, mask_dir, classes, show_mask=True):
  ## Get mask
  mask_list = mask_processing(mask_dir)
  
  img_list = [os.path.join(dp, f) for dp, dn, filenames in os.walk(img_dir) for f in filenames if os.path.splitext(f)[1] == '.JPG']
             # [os.path.join(dp, f) for dp, dn, filenames in os.walk(img_dir) for f in filenames if os.path.splitext(f)[1] == '.JPG']
  N_img = len(img_list)
  
  # Columns for the output dataframe with time_series for each tree/mask
  ts_columns = []
  for mask_i in range(len(mask_list)):
    for season in  classes :
      ts_columns.append("tree_" + str(mask_i+1) + "_" + season)
  rgb_columns = []
  for mask_i in range(len(mask_list)):
    for color in  ["R","G","B"] :
      rgb_columns.append("tree_" + str(mask_i+1) + "_" + color)
  # Initiate empty 3D array; dimensions: image, tree, pheno-class
  # Here the values of the class probabilities will be stored
  time_series = np.empty([N_img, len(mask_list), len(classes)]) * np.nan
  dates = [] # empty list for the dates  
  # Initiate empty 3D array; dimensions: image, tree, color channel
  rgb_array = np.empty([N_img, len(mask_list),3]) * np.nan # array for color channels (3)

  for img_i, img_path in enumerate(img_list):
    if (img_i + 1) % 10 == 0: # print status every 10 images
      print("Processing image: " + str(img_i+1) + "/" + str(N_img))
    # img_path_norm = os.path.normpath(img_path) # avoid mixed slahes in Windows
    # img_path_norm2 = img_path.replace("\\","/")
    img = tf.keras.preprocessing.image.load_img(img_path, target_size=IMG_SIZE)
    img_date = { ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS }['DateTime']
    img_array = tf.keras.preprocessing.image.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0)  # Create batch axis    
    ## Apply the mask and the image
    for mask_i in range(len(mask_list )):
      ## Set all non-mask pixels to 0
      cropped_image = tf.math.multiply(img_array, mask_list[mask_i][1])
      # crop the recatangle extent (height x width)of the maks
      cropped_image = tf.keras.layers.Cropping2D(cropping = mask_list[mask_i][0])(cropped_image)
      # Compute the mean of each color channel (RGB)
      # number of pixels in the mask
      mask_pixel_N = np.sum( mask_list[mask_i][1][0,:,:,0] )
      
      # compute mean of R, G, and B channel, save in array
      rgb_array[img_i, mask_i,:] = [np.sum(cropped_image[0,:,:,channel_i]) / mask_pixel_N for channel_i in [0,1,2]]
      
      ## Rescale to 512x512 - the required size for the model to predict
      # cropped_image= tf.image.resize(images=cropped_image, size=(512,512))
      cropped_image = tf.image.resize_with_pad(image = cropped_image,
                                       target_height = 512,
                                       target_width = 512)
      
      # Predict cropped image
      prediction = model.predict(cropped_image, verbose=0)[0]
      if len(prediction) == 1:
          prediction = [1-prediction, prediction]
      time_series[img_i, mask_i,:] = prediction
      
      
      ## For the first image show the cropped mask of eachtree
      if img_i == 0 and show_mask:
        plt.imshow(cropped_image[0].numpy().astype("uint8"))    
        plt.axis('off')
        plt.savefig(mask_dir.replace("mask.png", "tree_mask_id_") + str(mask_i + 1) + ".jpg")
        plt.close()
    # Append the date to the dataframe
    dates.append(img_date)
  
  shp = time_series.shape
  # reshape to 3D to 2D
  time_series = np.reshape(time_series, newshape=(shp[0], shp[1]*shp[2]), order="C")
  time_series = pd.concat([pd.Series(dates), pd.DataFrame(time_series)], axis=1)
  # RGB
  shp = rgb_array.shape
  rgb_array = np.reshape(rgb_array, newshape=(shp[0], shp[1]*shp[2]), order="C")
  # merge prediction and RGB values
  time_series = pd.concat([time_series, pd.DataFrame(rgb_array)], axis=1)
  # add column names
  time_series.columns = ["date"] + ts_columns + rgb_columns
  # format the dates
  time_series["date"] = [datetime.strptime(x, '%Y:%m:%d %H:%M:%S') for x in time_series.date]
  time_series = time_series.set_index(time_series.date)
  time_series = time_series.sort_index(axis=0)
  return(time_series)


''' 06 -  Apply the model to one folder
----------------------------------------------------------------------------'''

''' 6.1 - Full phenocam images 
----------------------------------------------------------------------------'''
def pheno_analyze(meta_dir, n_seasons):
  ## Load selected model
  print(n_seasons)
  model, classes = load_model(n_seasons = n_seasons)
  
  ## check input files
  all_subdirs = [x[0] for x in os.walk(meta_dir)]
  # Find each directory that contains a "mask.png" image
  # Directories with such a file will be identified as individual cameras.
  cam_dir_i = ["mask.png" in os.listdir(temp_dir) for temp_dir in  all_subdirs]
  cam_dirs = [i for (i, v) in zip(all_subdirs, cam_dir_i) if v]
  
  for cam_dir in cam_dirs:
    print("Anylyzing camera: " + cam_dir)
    # cam_dir = "/D/Work/Other_peoples_data/ExPlEco_phenology_pictures/inference_test_with_mask/a"
    # time_series = pheno_images_to_time_series(model = model,
    time_series = pheno_images_to_time_series_multi(
        model = model,
        img_dir = cam_dir,
        mask_dir = os.path.join(cam_dir,"mask.png"),
        classes = classes,
        show_mask=True
        )
    
    ''' Plot prediction time series - multi (>2) seasons'''
    trees = list(set( [x[:6]  for x in time_series.columns[1:] ] )) # unique tree ids
    for tree in trees:
      for season in classes:
        col_i = tree + "_" + season
        plt.plot(time_series[col_i],  label=col_i, color=color_dict[season])
      plt.title(tree)
      plt.ylabel("Probability")
      plt.hlines(0.5, min(time_series.date), max(time_series.date), color="grey")
      plt.legend()
      plt.xticks(rotation=45)
      plt.tight_layout()
      # plt.savefig(cam_dir + "/" + tree + ".jpg")
      plt.savefig( os.path.join(cam_dir, tree) +  ".jpg" , dpi=200)
      plt.show()
      plt.close("all")

    ''' Plot RGB time series '''
    rgb_colors = {"R":"red", "G": "green", "B":"steelblue"}
    for tree in trees:
      for color_i in rgb_colors.keys():
        col_i = tree + "_" + color_i
        plt.plot(time_series[col_i],  label=col_i, color=rgb_colors[color_i])
      plt.title(tree + " RGB")
      plt.ylim(0,255)
      plt.ylabel("Color value (0-255)")
      plt.xticks(rotation=45)
      plt.tight_layout()
      # plt.savefig(cam_dir + "/" + tree + "_RGB.jpg")
      plt.savefig( os.path.join(cam_dir, tree) +  "_RGB.jpg", dpi=200)
      plt.show()
      plt.close("all")
    
    ''' Save as CSV '''
    # time_series.to_csv(cam_dir + "/raw_time_series.csv", index=False)
    time_series.to_csv(os.path.join(cam_dir, "raw_time_series.csv") , index=False)

''' 6.2 - Cropped phenocam images, ie. single tree crowns
----------------------------------------------------------------------------'''
def pheno_analyze_cropped_crown(meta_dir, n_seasons):
  ## Load selected model
  model, classes = load_model(n_seasons = n_seasons)
  ## check input files
  all_subdirs = [x[0] for x in os.walk(meta_dir)]
  # Find each directory that contains a "mask.png" image
  # Directories with such a file will be identified as individual cameras.
  sub_dir_i = ["image_date_and_dataset.csv" in os.listdir(temp_dir) for temp_dir in  all_subdirs]
  sub_dirs = [i for (i, v) in zip(all_subdirs, sub_dir_i) if v]
  
  for sub_dir in sub_dirs:
    print("Anylyzing sub-directory: " + sub_dir)
    # sub_dir = cam_dirs[0]
    image_df = read_csv(os.path.join(sub_dir, "image_date_and_dataset.csv"))
    ## adding columns for: cam, filename, class probability per tree, RGB per tree
    image_df["cam"] = [x.split("/")[9] for x in image_df.img]
    image_df["file_name"] = [x.split("/")[11].replace(".JPG","") for x in image_df.img]
    ## add classification probabiliyt columns
    for j in range(1,8): # max number of masks is 7
        for c in classes:
            image_df["tree_"  + str(j) + "_" + c] = np.nan        
    ## add RGB columns
    for j in range(1,8): # max number of masks is 7
        image_df["tree_"  + str(j) + "_R"] = np.nan
        image_df["tree_"  + str(j) + "_G"] = np.nan
        image_df["tree_"  + str(j) + "_B"] = np.nan

    ## Loop over images; classify the image; save in df
    img_dir = os.path.join(sub_dir, "method_comparison")
    cropped_img_list = os.listdir(img_dir)
    image_n = len(cropped_img_list)
    for img_i, img_file_name in enumerate(cropped_img_list):
        if img_i%20  == 0: # Progress monitor
            print("Image " + str(img_i) + "/" + str(image_n))
        # load image and convert to array
        img = tf.keras.preprocessing.image.img_to_array(
            tf.keras.preprocessing.image.load_img(
                os.path.join(img_dir, img_file_name),
                target_size=(512,512))
            )
        img = tf.expand_dims(img, axis=0) # add batch dimension for prediction
        prediction = model.predict(img, verbose=0)[0]
        ## add prediction to the dataframe
        tree_id = int(img_file_name[37]) # tree mask id from file name
        cam_id = img_file_name.split("_")[1]
        img_name = img_file_name.split("_")[3]
        ## find row index of the corresponding (croppd) image in the dataframe of the full images
        df_i = np.where( (image_df.cam == cam_id) & (image_df.file_name == img_name) )[0]
        image_df.iloc[df_i]
        
        if len(classes) == 2:
            image_df.loc[df_i,"tree_" + str(tree_id) + "_summer"] = 1- prediction
            image_df.loc[df_i,"tree_" + str(tree_id) + "_winter"] = prediction
        else:
            image_df.loc[df_i,["tree_" + str(tree_id) + "_" + c for c in classes]] = prediction
        
        ## get mean RGB values
        #number of pixels that are not 0 on all 3 color channels
        mask_size = 512*512 - np.sum(np.logical_and(np.logical_and(img[0,:,:,0] == 0, img[0,:,:,1] == 0), img[0,:,:,2] == 0 ))
        image_df.loc[df_i,"tree_" + str(tree_id) + "_R"] = np.sum(img[0,:,:,0]) / mask_size
        image_df.loc[df_i,"tree_" + str(tree_id) + "_G"] = np.sum(img[0,:,:,1]) / mask_size
        image_df.loc[df_i,"tree_" + str(tree_id) + "_B"] = np.sum(img[0,:,:,2]) / mask_size
    
    ## only keep the columns from the method comparison
    image_df = image_df[image_df.dataset == "method_comparison"]
    ## sort by time
    image_df['time'] = pd.to_datetime(image_df['time'], format='%Y:%m:%d %H:%M:%S')
    image_df = image_df.sort_values(by=["cam","time"], axis=0)
    ## Save as csv
    image_df.to_csv(os.path.join(sub_dir, "image_date_and_dataset_analyzed.csv"), index=False)
    
    ## Create class-probability time-series plots
    all_cams = np.sort(image_df.cam.unique())
    for cam in all_cams:
        cam_df = image_df[ image_df["cam"] == cam ]
        ## loop over trees
        for j in range(1,8): # max number of (tree) masks is 7
            # check if the columns are empty (no such tree ID for this camera)    
            if not all(np.isnan(cam_df["tree_" + str(j) + "_summer"])):
                ## probabilities
                for c in classes:
                    plt.plot(cam_df.time, cam_df["tree_" + str(j) + "_" + c], label=c, color=color_dict[c])
                plt.xticks(rotation=45)
                plt.hlines(0.5, min(cam_df.time), max(cam_df.time), color="grey")
                plt.ylabel("Probability")
                plt.legend()
                plt.title(cam + ", Tree " + str(j))
                plt.tight_layout()
                plt.savefig( os.path.join(sub_dir, cam + "_tree_" + str(j) + ".jpg"), dpi=200 )
                plt.show()
                plt.close("all")
                
                ## RGB values
                plt.plot(cam_df.time, cam_df["tree_" + str(j) + "_R"], color="red")
                plt.plot(cam_df.time, cam_df["tree_" + str(j) + "_G"], color="green")
                plt.plot(cam_df.time, cam_df["tree_" + str(j) + "_B"], color="blue")
                plt.xticks(rotation=45)
                plt.ylim(0,255)
                plt.ylabel("Color value (0-255)")
                plt.title(cam + ", Tree " + str(j))
                plt.tight_layout()
                plt.savefig( os.path.join(sub_dir, cam + "_tree_" + str(j) + "_RGB.jpg"), dpi=200 )
                plt.show()
                plt.close("all")


''' 07 -  Compute confusion matrix for a given labeled dataset (typically the valiudation dataset)
----------------------------------------------------------------------------'''
def evaluate_accuracy(input_dir, n_seasons):
    model, classes = load_model(n_seasons = n_seasons)
    # init output dataframe
    results_df = pd.DataFrame(columns=["img","label","model_classification"])
    # confusion  matrix; labels in rows, model classification in columns
    conf_mtx= pd.DataFrame(np.nan, index=classes, columns=classes)
    for season in classes:
        img_dir = os.path.join(input_dir, season)
        img_list = os.listdir(img_dir)
        img_list = [k for k in img_list if 'JPG' in k]
        results_list = []
        for i,img_file_name in enumerate(img_list):
            if i%50 == 0: # loop-progress monitoring
                print("Season:" + season + " ; Image:" + str(i) + "/" + str(len(img_list)))
            # img_file_name = img_list[0]
            img = tf.keras.preprocessing.image.img_to_array(
                tf.keras.preprocessing.image.load_img(
                    os.path.join(img_dir, img_file_name),
                    target_size=(512,512))
                )
            img = tf.expand_dims(img, axis=0) # add batch dimension for prediction
            if len(classes) == 2:
                prediction = float(model.predict(img, verbose=0)[0])
                if prediction >= 0.5:
                    results_list.append(classes[1])
                else:
                    results_list.append(classes[0])
            else:
                prediction = [float(x) for x in model.predict(img, verbose=0)[0]]
                prediction = model.predict(img, verbose=0)[0]
                classes
                results_list.append( classes[np.argmax(prediction)] )
                
        results_df = pd.concat([
            results_df,
            pd.DataFrame({ "img":img_list,
                          "label":[season]*len(img_list), 
                          "model_classification":results_list })
            ])
    
    ## Compute confusion matrix
    for label in classes:
        for prediction in classes:
            conf_mtx.loc[label, prediction] = sum(np.logical_and(results_df.label == label, results_df.model_classification == prediction))
    ## relative confusion matrix values - NOT considering different sample sizes per class:
    conf_mtx_rel = np.array(conf_mtx) / sum(sum(np.array(conf_mtx)))
    ## relative confusion matrix values - CONSIDERING different sample sizes per class:
    conf_mtx_rel_class = pd.DataFrame(np.nan, index=classes, columns=classes)
    for label in classes:
        conf_mtx_rel_class.loc[label,:] = conf_mtx.loc[label,:] / sum(conf_mtx.loc[label,:])

    ## before saving; add label and prediction to column and row names:
    conf_mtx.columns = conf_mtx.columns + "_prediction"
    conf_mtx.index = conf_mtx.index + "_label"
    conf_mtx.to_csv(os.path.join(input_dir, "confusion_matrix_absolute.csv"))
    #
    # conf_mtx_rel = pd.DataFrame(conf_mtx_rel,index=classes, columns=classes)
    # conf_mtx_rel.columns = conf_mtx_rel.columns + "_prediction"
    # conf_mtx_rel.index = conf_mtx_rel.index + "_label"
    # conf_mtx_rel.to_csv(os.path.join(input_dir, "confusion_matrix_relative.csv"))
    #
    conf_mtx_rel_class.columns = conf_mtx_rel_class.columns + "_prediction"
    conf_mtx_rel_class.index = conf_mtx_rel_class.index + "_label"
    conf_mtx_rel_class.to_csv(os.path.join(input_dir, "confusion_matrix_season-relative.csv"))
    
    
    

'''############################################################################
###  Very simple graphical QT user interface
############################################################################'''
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QFrame, QVBoxLayout, QPushButton, QFileDialog, QLabel, QComboBox

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle("Phenology analyzer")
        self.window_width, self.window_height = 300, 250
        self.setMinimumSize(self.window_width, self.window_height)
        self.move(300, 300)
        self.setStyleSheet("QLabel{font-size: 10pt;}")
        
        self.input_dir = None
        
        frame =QFrame()
        self.setCentralWidget(frame)
        layout = QVBoxLayout()
        frame.setLayout(layout)
        
        ''' Select model '''
        self.label_model = QLabel("Select model")
        layout.addWidget(self.label_model)  
        self.ButtonSelectModel = QComboBox(self)
        self.ButtonSelectModel.addItem("2 seasons")
        self.ButtonSelectModel.addItem("3 seasons")
        layout.addWidget(self.ButtonSelectModel)
        
        ''' Select full or cropped image '''
        self.label_input_type = QLabel("Select input type")
        layout.addWidget(self.label_input_type)  
        self.ButtonSelectInputType = QComboBox(self)
        self.ButtonSelectInputType.addItem("Full image")
        self.ButtonSelectInputType.addItem("Cropped tree crown image")
        layout.addWidget(self.ButtonSelectInputType)
        
        ''' Input label '''
        ## Label
        self.label_input = QLabel("Input directory: ")
        layout.addWidget(self.label_input)  
        ## Button
        self.ButtonOpen = QPushButton('Select an INPUT folder',self)
        layout.addWidget(self.ButtonOpen)
        self.ButtonOpen.clicked.connect(self.buttonInputClicked)
        
        ''' Analyze '''
        ## Button
        self.ButtonAnalyze= QPushButton('Analyze',self)
        layout.addWidget(self.ButtonAnalyze)
        self.ButtonAnalyze.clicked.connect(self.buttonAnalyzeClicked)

        ''' Evaluate accuracy and compute confusion matrix '''
        ## Label
        self.label_conf_mtx = QLabel("Compute the confusion matrix for a labeled dataset")
        layout.addWidget(self.label_conf_mtx)  
        ## Button
        self.ButtonEvaluate= QPushButton('Compute confusion matrix',self)
        layout.addWidget(self.ButtonEvaluate)
        self.ButtonEvaluate.clicked.connect(self.buttonEvaluateClicked)

    def buttonInputClicked(self):
      self.input_dir = QFileDialog.getExistingDirectory(
          self,
          caption='Select an INPUT folder'
      )
      if self.input_dir:
        self.label_input.setText("Input directory:\n" + self.input_dir)
        
    def buttonAnalyzeClicked(self):
      if self.input_dir and str(self.ButtonSelectInputType.currentText()) == "Full image":
        print("Processing: " + self.input_dir)
        pheno_analyze(self.input_dir, str(self.ButtonSelectModel.currentText()))
      elif self.input_dir and str(self.ButtonSelectInputType.currentText()) == "Cropped tree crown image":
        print("Processing: " + self.input_dir)
        pheno_analyze_cropped_crown(self.input_dir, str(self.ButtonSelectModel.currentText()))
      else:
        print("No directory selected")
        print(str(self.ButtonSelectModel.currentText()))
        
    def buttonEvaluateClicked(self):
        print("Computing accuracy and confusion matrix for: " + self.input_dir)
        evaluate_accuracy( self.input_dir, str(self.ButtonSelectModel.currentText()) )

def main():
    app = QApplication([])
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    PhenoApp = MainWindow()
    PhenoApp.show()

    try:
        sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')