#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Phenological classification of forest images

Essentially this script follows the Keras standard example and uses
the Xception model

Created on Tue Apr 26 10:07:56 2022

@author: Mario
"""

import tensorflow as tf
from tensorflow import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
from random import sample
from PIL import Image, ExifTags
from datetime import datetime

''' Settings
----------------------------------------------------------------------------'''
''' 2 Seasons, as discussed 09.2022'''
train_data_dir = "/D/Work/Other_peoples_data/ExPlEco_phenology_pictures/output_cropping_Antonia_5.10.2022/train"
validation_data_dir = "/D/Work/Other_peoples_data/ExPlEco_phenology_pictures/output_cropping_Antonia_5.10.2022/validation"
''' 3 Seasons - only very rough proof-of_concept training data'''
# train_data_dir = "/D/Work/Other_peoples_data/ExPlEco_phenology_pictures/train_data_cropped_with_mask_3_seasons/train"
# validation_data_dir = "/D/Work/Other_peoples_data/ExPlEco_phenology_pictures/train_data_cropped_with_mask_3_seasons/validation"

model_dir = "/home/mario/Dokumente/Python/Phenology_from_images/"
os.chdir(model_dir)

''' memory issues tests '''
physical_devices = tf.config.list_physical_devices('GPU')
try:
  tf.config.experimental.set_memory_growth(physical_devices[0], True)
except:
  # Invalid device or cannot modify virtual devices once initialized.
  pass


''' 01 - Generate dataset
----------------------------------------------------------------------------'''
IMG_SIZE = (512, 512)
batch_size = 8 # 32
# Generates a TF Dataset
train_ds = tf.keras.preprocessing.image_dataset_from_directory(
    directory = train_data_dir,
    labels='inferred',
    shuffle=True,
    image_size=IMG_SIZE,
    batch_size=batch_size,
    follow_links=True
)

validation_ds = tf.keras.preprocessing.image_dataset_from_directory(
    directory = validation_data_dir,
    labels='inferred',
    shuffle=True,
    image_size=IMG_SIZE,
    batch_size=batch_size,
    follow_links=True
)

N_classes = len(train_ds.class_names)

# # we have very different sample sizer per class/season, thus weight the samples
# class_weight = {"winter": 0.33,  # many winter picture, lowest weight/importance
#                 "summer":0.66,   # medium amount of summer pictures
#                 "autumn": 1.     # only few autumn pictures, thus gove them more weight
#                 }


''' 02 - Show example images
----------------------------------------------------------------------------'''
# plt.figure(figsize=(10, 10))
# for images, labels in train_ds.take(1):
#     for i in range(4):
#         ax = plt.subplot(2, 2, i + 1)
#         plt.imshow(images[i].numpy().astype("uint8"))
#         plt.title(int(labels[i]))
#         plt.axis("off")


''' 03 - Data augmentation (random flips, rotation...)
         and prepocessing for ResNet (color-value standardization)
----------------------------------------------------------------------------'''
data_augmentation = tf.keras.Sequential(
    [
      tf.keras.layers.RandomFlip("horizontal_and_vertical"),
      tf.keras.layers.RandomRotation(0.15)
    ]
)

''' 04 a - Build and compile Xception model
----------------------------------------------------------------------------''' 
# train_ds = train_ds.prefetch(buffer_size=8) # optimize for perfomance (loading images)
def make_model(input_shape, num_classes):

    inputs = keras.Input(shape=input_shape)
    # Image augmentation block
    x = data_augmentation(inputs)

    # Entry block
    x = tf.keras.layers.Rescaling(1.0 / 255)(x)
    x = tf.keras.layers.Conv2D(32, 3, strides=2, padding="same")(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Activation("relu")(x)

    x = tf.keras.layers.Conv2D(64, 3, padding="same")(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Activation("relu")(x)

    previous_block_activation = x  # Set aside residual

    for size in [128, 256, 512, 728]:
    # for size in [32, 64, 128]:
        x = tf.keras.layers.Activation("relu")(x)
        x = tf.keras.layers.SeparableConv2D(size, 3, padding="same")(x)
        x = tf.keras.layers.BatchNormalization()(x)

        x = tf.keras.layers.Activation("relu")(x)
        x = tf.keras.layers.SeparableConv2D(size, 3, padding="same")(x)
        x = tf.keras.layers.BatchNormalization()(x)

        x = tf.keras.layers.MaxPooling2D(3, strides=2, padding="same")(x)

        # Project residual
        residual = tf.keras.layers.Conv2D(size, 1, strides=2, padding="same")(
            previous_block_activation
        )
        x = tf.keras.layers.add([x, residual])  # Add back residual
        previous_block_activation = x  # Set aside next residual

    x = tf.keras.layers.SeparableConv2D(1024, 3, padding="same")(x)    
    # x = tf.keras.layers.SeparableConv2D(256, 3, padding="same")(x)
    
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Activation("relu")(x)

    x = tf.keras.layers.GlobalAveragePooling2D()(x)
    if num_classes == 2:
        activation = "sigmoid"
        units = 1
    else:
        activation = "softmax"
        units = num_classes

    x = tf.keras.layers.Dropout(0.5)(x)
    outputs = tf.keras.layers.Dense(units, activation=activation)(x)
    return keras.Model(inputs, outputs)


model = make_model(input_shape = IMG_SIZE + (3,), num_classes = N_classes)
keras.utils.plot_model(model, show_shapes=True)
callbacks = [
    keras.callbacks.ModelCheckpoint("save_at_epoch_{epoch}.h5"),
]

# loss_fn = keras.losses.SparseCategoricalCrossentropy(from_logits=True)

model.compile(
    optimizer=keras.optimizers.Adam(1e-3),
    metrics=["accuracy"],
    loss="binary_crossentropy", # for 2 classes
    # loss="sparse_categorical_crossentropy" # for > 2 classes
    # loss="categorical_crossentropy",
    )

''' 05 - Train the model
----------------------------------------------------------------------------''' 
history = model.fit(
    train_ds, epochs=20, validation_data = validation_ds, callbacks = callbacks
)

## For development: load specified model weights
# model.load_weights('/home/mario/Dokumente/Python/Phenology_from_images/save_at_epoch_14.h5')
# model.load_weights('/home/mario/Dokumente/Python/Phenology_from_images/saved_models/2022_05_11_08_50_41Xception_default/save_points/save_at_epoch_10.h5')
# 
## Save the model
# now = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
# save_name = model_dir + 'saved_models/' + now + 'Xception_2_seasons_epoch_14'
# model.save(save_name) # unique save point

## Load a model
# save_name = '2022_05_06_13_53_05_MODEL_exception_already-augmented_images_epochs_10'
# model = tf.keras.models.load_model( save_name )

''' 06 - Plot training history and statistcs
----------------------------------------------------------------------------''' 
hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch
plt.plot(hist.epoch, hist.accuracy)
plt.plot(hist.epoch, hist.val_accuracy)
plt.vlines(13,0.97,1)
plt.savefig(save_name + "/accuracy.png")